import { createSlice, configureStore } from '@reduxjs/toolkit'
import itemSlice from './redux/itemSlice'
import cartSlice from './redux/cartSlice';
import modalSlice from './redux/modalSlice';
// import favSlice from './redux/favSlice';
const store = configureStore({
    reducer : {
        items: itemSlice,
        cart: cartSlice,
        modal: modalSlice,
        // fav: favSlice,
    }
})

export default store;