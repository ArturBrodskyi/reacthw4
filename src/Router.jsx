import { Routes, Route } from "react-router-dom";
import HomeRoute from "./components/HomeRoute";
import CartRoute from "./components/CartRoute";
import FavRoute from "./components/FavRoute";
import { useSelector } from "react-redux";

const Router = ({
  favourite,
  removeFromCart,
  favNumber,
  FavouriteFn = () => {},
  openModal = () => {},
  addToCart = () => {},
  cartNumber,
  openDeleteModal,
  items
}) => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomeRoute
          items= {items}
            FavouriteFn={FavouriteFn}
            openModal={openModal}
            addToCart={addToCart}
            cartNumber={cartNumber}
            favNumber={favNumber}
            favourite = {favourite}
          />
        }
      ></Route>
      <Route
        path="/cart"
        element={
          <CartRoute
            removeFromCart={removeFromCart}
            openDeleteModal={openDeleteModal}
          />
        }
      ></Route>
      <Route
        path="/favourite"
        element={
          <FavRoute
            openModal={openModal}
            FavouriteFn={FavouriteFn}
          ></FavRoute>
        }
      ></Route>
    </Routes>
  );
};

export default Router;
