import propTypes from "prop-types";
import ItemCart from "../ItemCart";
import { useSelector } from "react-redux";

const ItemCartList = ({ removeFromCart, openDeleteModal }) => {
  const items = useSelector(store => store.cart.notes);
  console.log(items);
  return (
    
    <div className="itemContainer">
      {items && items.map((item) => item && <ItemCart key={item.articul}  item = {item} removeFromCart={removeFromCart} openDeleteModal={openDeleteModal} />)}
    </div>
  );
};

ItemCartList.propTypes = {
  items: propTypes.array,
};

export default ItemCartList;