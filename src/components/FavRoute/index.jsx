import ItemList from "../itemList";
import { useSelector } from "react-redux";
const FavRoute = ({ FavouriteFn, openModal}) => {
  const favItems = useSelector(state => state.favData)
  return (
    <>
    
      <ItemList
        items={favItems}
        FavouriteFn={FavouriteFn}
        openModal={openModal}
      ></ItemList>
    </>
  );
};

export default FavRoute;
