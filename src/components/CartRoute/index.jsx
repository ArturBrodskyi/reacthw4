import ItemCartList from "../ItemCartList";

const CartRoute = ({ removeFromCart, openDeleteModal}) => {
    return(
<ItemCartList removeFromCart={removeFromCart} openDeleteModal={openDeleteModal}></ItemCartList>
    )
}

export default CartRoute;