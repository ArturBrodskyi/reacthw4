import PropTypes from 'prop-types';
import StarSvg from '../svg/StarSvg';
import classNames from 'classnames';
import Button from '../Button';
import { useDispatch } from 'react-redux';
import { addToFavourites, favouriteFn } from '../../redux/itemSlice';

const Item = ({ item = {}, articul, isFavourite = false, openModal = () => {} }) => {
    const dispatch = useDispatch();

    const addToFavourite = (item) => {
        dispatch(addToFavourites(item))
    };

    return (
        <div className='item'>
            <img className='item-img' src={item.img} alt="" />
            <h1 className='item-name'>{item.name}</h1>
            <h2 className='item-price'>{item.price}</h2>
            <div className='item-btns'>
                <Button className='add-to-cart' onClick={openModal}>Add to cart</Button>
                <div
                    className={classNames('add-to-favourites', { 'favourite-active': isFavourite })}
                    onClick={() => addToFavourite(item)}
                >
                    <StarSvg />
                </div>
            </div>
        </div>
    );
};

Item.propTypes = {
    item: PropTypes.object,
    articul: PropTypes.string.isRequired,
    isFavourite: PropTypes.bool,
    openModal: PropTypes.func,
};

export default Item;
