import propTypes from 'prop-types'

const ItemCart =({item, openDeleteModal}) =>{
    
    return(
        <div className='item'>
          <img className='item-img' src={item.img} alt="" />
            <h1 className='item-name'>{item.name}</h1>
            <h2 className='item-price'>{item.price}</h2>
        <button onClick={()=>openDeleteModal(item)}>Remove from cart</button>
        </div>

    )
}

ItemCart.propTypes ={
    item: propTypes.object,
}

export default ItemCart;