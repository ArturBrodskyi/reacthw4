import ItemList from "../itemList";
import Counter from "../Cart&FavCounter";
const HomeRoute = ({ FavouriteFn = () => {}, openModal = () => {}, addToCart = () => {}, cartNumber, favNumber, items }) => {
  return (
    <>
      <Counter favNumber={favNumber} cartNumber={cartNumber}></Counter>
      <ItemList
        items={items}
        FavouriteFn={FavouriteFn}
        openModal={openModal}
        addToCart={addToCart}
      ></ItemList>
    </>
  );
};

export default HomeRoute;
