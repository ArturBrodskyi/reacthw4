import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const url = './arr.JSON';
const favUrl = './favArr.JSON';

export const addToFavourites = createAsyncThunk(
  'fav/addToFavourites',
  async ({articul}) => {; 
    const item = state.data.find(item => item.articul === articul);
    if (item) {
      const { data } = await axios.post(favUrl, { item });
      return data;
    }
  }
);

export const fetchFavourites = createAsyncThunk(
  'fav/fetchFavourites',
  async () => {
    const { data } = await axios.get(favUrl);
    return data;
  }
);

export const fetchProducts = createAsyncThunk(
  'items/fetchProducts',
  async () => {
    const { data } = await axios.get(url);
    return data;
  }
);

const initialState = {
  data: [],
  favData: []
};

const itemsSlice = createSlice({
  name: 'items',
  initialState,
  reducers: {
    favouriteFn(state, action) {
      const { articul } = action.payload;
      const item = state.data.find(item => item.articul === articul);
      if (item) {
        item.isFavourite = !item.isFavourite;
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchProducts.fulfilled, (state, action) => {
      state.data = action.payload;
    });
    builder.addCase(addToFavourites.fulfilled, (state, action) => {
      state.favData.push(action.payload);
    });
    builder.addCase(fetchFavourites.fulfilled, (state, action) => {
      state.favData = action.payload;
    });
  }
});

export const { favouriteFn } = itemsSlice.actions;
export default itemsSlice.reducer;
