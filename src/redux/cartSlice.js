import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  notes: [],
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addItem(state, action) {
      state.notes.push(action.payload);
    },
    removeItem(state, action) {
      state.notes = state.notes.filter(
        (item) => item.articul !== action.payload
      );
    },
  },
});

export const { addItem, removeItem} = cartSlice.actions;
export default cartSlice.reducer;
