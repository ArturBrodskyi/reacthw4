import { useState, useEffect } from "react";
import { useImmer } from "use-immer";
import axios from "axios";
import ItemList from "./components/itemList";
import "./App.scss";
import Counter from "./components/Cart&FavCounter";
import Modal from "./components/Modal";
import Router from "./Router.jsx";
import Header from "./components/Header";
import { useDispatch, useSelector } from "react-redux";
import { addItem, removeItem,  } from "./redux/cartSlice.js";
import { favouriteFn, fetchFavourites, fetchProducts } from "./redux/itemSlice.js";
import { openModalSlice, closeModalSlice, openDeleteModalSlice, closeDeleteModalSlice } from "./redux/modalSlice.js";
// import { favFunction } from "./redux/favSlice.js";

function App() {
  const cartItems = useSelector(state => state.cart.notes)
  const items = useSelector(state => state.items.data)
  const modalState = useSelector(state => state.modal)
  const selectedItem = modalState.selectedItem;
  const isModalOpen = modalState.isModalOpen
  const isDeleteModalOpen = modalState.isDeleteModalOpen

  
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchProducts())
  }, [])

  useEffect(() => {
    dispatch(fetchFavourites())
  }, [])

  const openModal = (item) => {
    dispatch(openModalSlice(item))
  };

  const closeModal = (item) => {
    dispatch(closeModalSlice(item))
  };

  const openDeleteModal = (item) => {
    dispatch(openDeleteModalSlice(item))
  }

  const closeDeleteModal = (item) => {
    dispatch(closeDeleteModalSlice(item))
  }

  const addToCart = (item) => {

    const cartItem = items.find(({ articul }) => articul === item.articul);
    dispatch(addItem(cartItem))
    closeModal();
  };
  const removeFromCart = (itemToDelete) => {
    const deletedItem = itemToDelete.articul
    dispatch(removeItem(deletedItem));

    closeDeleteModal()
  };


  const FavCounter = () => {
    const favourite = cartItems.filter((item) => item.isFavourite === true);
    const favNumber = favourite.length;
    return { favNumber, favourite };
  };
  const { favNumber, favourite } = FavCounter();
  return (
    <>
      <Header></Header>
      <Router
        items={items}
        openModal={openModal}
        addToCart={addToCart}
        removeFromCart={removeFromCart}
        openDeleteModal={openDeleteModal}
        favNumber = {favNumber}
        favourite = {favourite}
        
      ></Router>

      {selectedItem && (
        <Modal
          headerCont="Add to cart?"
          bodyCont={`Add ${selectedItem.name} to cart?`}
          firstButtonText="Add to cart"
          addToCart={() => addToCart(selectedItem)}
          className="fav-modal"
          isModalOpen={isModalOpen}
          onCloseClick={closeModal}
        ></Modal>
      )}
      {selectedItem && (
      <Modal
        headerCont="Remove from cart?"
        bodyCont={`Remove ${selectedItem.name} from cart?`}
        firstButtonText="Ok"
        removeFromCart={() => removeFromCart(selectedItem)}
        className="fav-modal"
        isDeleteModalOpen={isDeleteModalOpen}
        closeDeleteModal={closeDeleteModal}
      ></Modal>
    )}
    </>
  );
}
export default App;